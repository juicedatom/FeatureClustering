%function T = betterOlson(map, scan, pixSrch, moveRange, lidarStd, pixRadius, lidarRange)
mag = @(a,b) sqrt((a(1) - b(1))^2 + (a(2) - b(2))^2);
%clear
clc
close all

nScanIndex = unique(Lidar_ScanIndex);

lidarRange = 2;

map = [];
pose = [0 0 0];
world = [];

figure(1)

T = [0 0 0];

for scanIdx=800:10:8000
	tic
    disp 'rasaterizing'
    scan = getLidarXY(scanIdx, nScanIndex, Lidar_Angles, Lidar_Ranges, Lidar_ScanIndex);

    pts = [];
    %n = 50;
    ptDist = 0.01;
    thresh = 0.05;

    res = deg2rad(size(scan,1)/270);
    theta = (pi-2*res)/2;

    normalizer = 1;
    alpha = sin(res)/sin(theta)*normalizer;
    d_max = alpha * 30;

    minlen = 0.4;
    found = 0;
	edges = [];

	startpt = [];
	endpt = [];

	adjscan = [];

    for idx = 1:size(scan,1) - 1

    	a = scan(idx,:);
    	b = scan(idx+1,:);	

    	%get minimum distance for a point to match up.
    	thresh = alpha * mag([0,0], (a + b) / 2);

		adjscan = [adjscan; a; b;];

    	if mag(a,b) <= thresh;

    		if found == 0
    			edges = [edges; a];
    			found = 1;
    		end

    		n = ceil(mag(a,b)/ptDist);
    		x = linspace(a(1),b(1),n)';
    		y = linspace(a(2),b(2),n)';

    		adjscan = [adjscan; x y];
    	else
    		if found == 1
    			edges = [edges; a];
    		end
    		found = 0;
    	end
    end

    %filter scan to cancel out some pts

    figure(1);
    hold on
    cla
    axis([-2.5 2.5 -5 15])
    plot(adjscan(:,1), adjscan(:,2), 'b.');
    plot(scan(:,1), scan(:,2), 'g.');
    plot(edges(:,1), edges(:,2), 'rx');
    hold off
    axis equal

    figure(2);
    clf
    dat = [-adjscan(:,2), adjscan(:,1)];

    scanRange = max(dat) - min(dat);
    binSize = 0.03;

    I = hist3(dat, ceil(scanRange/binSize));
    I = imcomplement(I);
    trans = fspecial('gaussian', 4, 20);
    I = imfilter(I,trans);
    imshow(I);
	C = corner(I);
	hold on
	plot(C(:,1), C(:,2), 'r+');
	hold off


	%lines = houghlines(I,theta,rho,P);
	toc
    pause(0.1)
end