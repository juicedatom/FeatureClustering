function ret = rasterPt(data, offset, pixRadius)

N = size(data,1);

data = [-data(:,2), data(:,1)];
data = ceil(data/pixRadius) - repmat(offset,N,1);
ret = [data(:,2) data(:,1)];

end