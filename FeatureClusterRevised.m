%function T = betterOlson(map, scan, pixSrch, moveRange, lidarStd, pixRadius, lidarRange)
mag = @(a,b) sqrt((a(1) - b(1))^2 + (a(2) - b(2))^2);
%clear
clc
close all

nScanIndex = unique(Lidar_ScanIndex);

lidarRange = 2;

map = [];
pose = [0 0 0];
world = [];

figure(1)

T = [0 0 0];

pixRadius = 0.03;

for scanIdx=800:10:8000
	tic
    disp 'rasaterizing'
    scan = getLidarXY(scanIdx, nScanIndex, Lidar_Angles, Lidar_Ranges, Lidar_ScanIndex);

    [adjscan, E] = fillLidarData(scan, 30, 270);
    [raster, offset] = rasterizeData(adjscan, 5, 10, pixRadius);

    subplot(1,2,1);
    cla
    C = corner(raster);
    imshow(raster);
    hold on
    plot(C(:,1),C(:,2),'bx');
    E_pix = rasterPt(E, offset, pixRadius);
    plot(E_pix(:,1),E_pix(:,2),'gx');
    hold off

    %filter scan to cancel out some pts

    subplot(1,2,2);
    hold on
    cla
    axis([-2.5 2.5 -5 15])
    plot(adjscan(:,1), adjscan(:,2), 'b.');
    plot(scan(:,1), scan(:,2), 'g.');
    plot(E(:,1),E(:,2),'rx');
    hold off
    axis equal

	toc
    pause(0.1)
end