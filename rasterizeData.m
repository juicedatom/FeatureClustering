function [raster, minDisp] = rasterizeData(data, pixSrch, lidarRange, pixRadius)
    disp 'rasaterizing'
    data = [-data(:,2), data(:,1)];

    minDat = min(data);
    maxDat = max(data);

    scanRange = maxDat - minDat;

    I = hist3(data, ceil(scanRange/pixRadius));
    I = imcomplement(I);

    minDisp = floor(minDat/pixRadius);

    trans = fspecial('gaussian', pixSrch, 5);
    raster = imfilter(I,trans);
end